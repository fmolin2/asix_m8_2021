= Pràctica servidor FTP

:encoding: utf-8
:lang: ca
:toc: left
:!numbered:
//:teacher:

ifdef::teacher[]
== (Versió del professor):
endif::teacher[]

<<<

== Objectiu de la pràctica

En aquesta pràctica configurarem un servidor FTP. L'objectiu és tenir un
espai de només lectura on compartir documents amb accés obert, un espai
compartit accessible des de la xarxa interna, i un espai propi per a cadascun
dels usuaris del sistema. També implantarem una capa SSL per tal de transmetre
la informació xifrada (Nou 2021 - Ho farem a M17).

== Preparatius

* Utilitzarem l'entorn de xarxa de les pràctiques anteriors. Crearem i configurarem
_Fry_ (172.30.100.3) com a servidor FTP.

* Qualsevol màquina podrà actuar com a client del FTP. Si tenim el servei DNS
funcionant, podrem accedir-hi directament pel nom del servidor, en cas contrari
ho haurem de fer utilitzant la seva IP.

* Si no tenim el servei DHCP funcionant, haurem d'assignar una IP fixa a PC1
per tal que es pugui connectar a la xarxa.

* En aquesta pràctica, enlloc de configurar la interfície 1 com a NAT, la especificarem com a *adaptador pont*

* Seguint els passos de les pràctiques anteriors, totes les màquines han de
mostrar un prompt personalitzat i hi ha d'haver un usuari amb el vostre nom i
permisos per utilitzar *sudo*.

[IMPORTANT]
====
Cal que configureu el vostre terminal perquè al prompt aparegui el vostre nom i
la data ressaltats en diferents colors. *Això és imprescindible per avaluar
les captures*.
====

== Apartat 1: Instal·lador del servidor FTP a _Fry_

Utilitzarem el programa *vsFTPd* (_very secure FTP daemon_). A diferència
d'altres programes d'FTP, el _vsFTPd_ incorpora diverses característiques de
seguretat, com no permetre la connexió a l'usuari _root_, o permetre, de forma
senzilla, que un usuari no pugui navegar per tot el sistema de fitxers del
servidor.

Amb la configuració per defecte el servidor FTP ja permet l'accés a qualsevol
usuari del *sistema*. Per comprovar-ne el funcionament, utilitzarem una màquina
en el mateix segment de xarxa.

La instal·lació base de Ubuntu ja inclou un client de FTP en línia d'ordres
(després ja passarem a client gràfic) que s'anomena, com és d'esperar, _ftp_.

Crea un fitxer de text al teu _home_ de _Fry_. Mira l'ajuda de l'ordre
_ftp_ i comprova que des del client pots connectar al servei FTP de _Fry_
utilitzant el teu nom d'usuari i contrasenya. Descarrega't el fitxer que acabes
de crear.

.Entregar
====
*Captura de la transacció FTP entre el client i _Fry_.*
====

== Apartat 2: Configuració de l'accés anònim

La primera tasca a realitzar és permetre l'accés anònim (de només lectura).

Per això hem de crear primer un directori on es deixaran els fitxers públics i
on accedirà l'usuari anònim.

Crearem el directori _ftpusers_ dins de _/home_. I dins de _ftpusers_ crearem
un directori per cada usuari del servei FTP (que no sigui un usuari del
sistema). En aquest cas crearem el directori _anonymous_ per l'usuari
anònim. El directori _anonymous_ ha de tenir com a grup propietari el grup
_ftp_.

ifndef::teacher[]
.Entregar
====
*Captura de la sortida de `ls -ld` sobre el nou directori.*
====
endif::teacher[]

Ara activem l'accés anònim editant el fitxer de configuració
_/etc/vsftpd.conf_. Cal configurar les opcions `anonymous_enable` i
`anon_root`.

ifndef::teacher[]
.Entregar
====
*Captura de les modificacions a la configuració.*
====
endif::teacher[]

Un cop activat l'accés anònim, crea un fitxer dins del seu _home_. Comprova
que el fitxer es pot descarregar accedint com usuari anònim des de l'ordinador
físic.

ifndef::teacher[]
.Entregar
====
*Captura d'una transacció utilitzant l'accés anònim.*
====
endif::teacher[]

== Apartat 3: Separació de la configuració del FTP en xarxes

A la nostra configuració final voldrem que l'usuari anònim pugui connectar des
de qualsevol lloc, però que la resta d'usuaris només ho puguin fer si es
connecten des de la xarxa local.

Per aconseguir això hem de crear dos fitxers de configuració (per exemple vsftpd.lan.conf i vsftpd.wan.conf) i indicar-li al
vsFTPd quin fitxer aplicar depenent d'on ve la connexió (LAN aplicaria en la xarxa 172.30.100.0/24 i WAN aplicaria si connectem des de la màquina física).

Aquest apartat utilitza les idees de configuració que s'expliquen al fitxer
_/usr/share/doc/vsftpd/examples/PER_IP_CONFIG_ de la pròpia instal·lació
del vsFTPd.

Fixa't en l'explicació d'aquest exemple i crea dues còpies del fitxer de
configuració actual, una que s'apliqui a la xarxa interna i l'altra als
accessos des de l'exterior. Només cal afegir una opció a la configuració i
crear el fitxer _hosts.allow_.

Per veure si ens està funcionant correctament, modificarem el missatge de
benvinguda a cadascun dels fitxers de configuració i comprovar que surt el
missatge corresponent segons l'origen de la connexió. El missatge de
benvinguda es configura a l'opció _ftpd_banner_.

ifndef::teacher[]
.Entregar
====
*Canvis al fitxer de configuració*
====
endif::teacher[]
ifdef::teacher[]
.Solució
====
----
# Activem la configuració diferenciada segons l'adreça IP:
tcp_wrappers=YES
----
====
endif::teacher[]

ifndef::teacher[]
.Entregar
====
*Fitxer /etc/hosts.allow*
====
endif::teacher[]
ifdef::teacher[]
.Solució
====
----
# /etc/hosts.allow: list of hosts that are allowed to access the system.
#                   See the manual pages hosts_access(5) and hosts_options(5).
#
# Example:    ALL: LOCAL @some_netgroup
#             ALL: .foobar.edu EXCEPT terminalserver.foobar.edu
#
# If you're going to protect the portmapper use the name "rpcbind" for the
# daemon name. See rpcbind(8) and rpc.mountd(8) for further information.
#

vsftpd: 172.30.100.0/24: setenv VSFTPD_LOAD_CONF /etc/vsftpd.lan.conf
vsftpd: 172.30.2.0/24: setenv VSFTPD_LOAD_CONF /etc/vsftpd.lan.conf
vsftpd: 172.30.3.0/24: setenv VSFTPD_LOAD_CONF /etc/vsftpd.lan.conf
----
====
endif::teacher[]

ifndef::teacher[]
.Entregar
====
*Comprovació accés exterior*
====
endif::teacher[]
ifdef::teacher[]
.Solució
====
----
$ ftp 192.168.1.90
Connected to 192.168.1.90.
220 Accés FTP. Usuaris externs.
Name (192.168.1.90:joan): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp>
----
====
endif::teacher[]

ifndef::teacher[]
.Entregar
====
*Comprovació accés intern*
====
endif::teacher[]
ifdef::teacher[]
.Solució
====
----
$ ftp 172.30.100.2
Connected to 172.30.100.2.
220 Accés FTP. Usuaris interns.
Name (172.30.100.2:root): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp>
----
====
endif::teacher[]


